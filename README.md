./mvnw clean install
docker build -t aws-docker-app .
docker image ls
docker run -d -p 8080:8080 aws-docker-app
docker container ls
docker container exec -it <<ID>> sh
# hit "Ctrl + p + q" to safe exit terminal
docker container stop <<ID>>
